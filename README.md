## 环境
python3.6.5

pip3

Flask


## virtualenv
```
启动
. pyenv/bin/activate


退出
deactivate
```


## 启动Flask项目
```python3 run.py```


## requirements操作
```
安装依赖
(pyenv) $ pip3 install -r requirements.txt

生成（更新）依赖
(pyenv) $ pip3 freeze > requirements.txt
```