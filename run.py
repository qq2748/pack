from application import app

if __name__ == '__main__':
	app.run(host='0.0.0.0', debug=True, port=4003, extra_files=app.config["OTA_APP_WATCH_DIRS"])
