from flask import render_template, Blueprint


demo = Blueprint('demo', __name__, template_folder='templates' , static_folder='static')


@demo.route('/')
def index():
	return render_template('demo/index.html')
	

@demo.route('/test')
def test():
	return render_template('demo/test.html')