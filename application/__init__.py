from flask import Flask
from .ota.ota_views import ota
from .demo.demo_views import demo
from .pack.pack_views import pack

app = Flask(__name__)
app.config.from_object('config')

app.register_blueprint(ota, url_prefix='/ota')
app.register_blueprint(demo, url_prefix='/demo')
app.register_blueprint(pack, url_prefix='/pack')

from application import views