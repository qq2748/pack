# from flask import current_app
from config import OTA_APP_ROOT as OTA_DIR
from flask import render_template, request, abort, redirect, send_file, url_for, Blueprint
from werkzeug import secure_filename
from . import ota_api
import os, re
import base64


ota = Blueprint('ota', __name__, template_folder='templates')


nav_list = ota_api.get_app_folders(OTA_DIR)


@ota.route('/')
def index():
	return render_template('ota/index.html', nav = nav_list)


@ota.route('/list/', defaults={'req_path': ''})
@ota.route('/list/<path:req_path>')
def list(req_path):
	pathList = re.split('/', req_path)
	path = os.path.join(OTA_DIR, os.path.join(*pathList))
	if os.path.exists(path):
		app_list = ota_api.get_list_inPath(path)
		return render_template('ota/list.html', nav = nav_list, applist = app_list)
	else:
		abort(404)
	end


@ota.route('/app/', defaults={'req_path': ''})
@ota.route('/app/<path:req_path>')
def app(req_path):
# 	浏览安装包信息
	pathList = re.split('/', req_path)
	path = os.path.join(OTA_DIR, os.path.join(*pathList))
	if os.path.isfile(path + '.ipa'):
		appInfo = ota_api.get_appInfo(path + '.ipa')
		
		# 使用urlsafe_b64encode得到不含有+、/等url不支持的字符，替换为_和-
		# 替换删除=，会在url出现歧义
		bytesString = str(req_path).encode(encoding="utf-8")
		encodestr = base64.urlsafe_b64encode(bytesString).decode().replace('=','')
		
		plistURL = request.scheme + "://" + request.host + url_for('ota.plist', req_path = encodestr)
		
		return render_template('ota/detail.html', nav = nav_list, app = appInfo, plistURL = plistURL)
	elif os.path.isfile(path + '.apk'):
		appInfo = ota_api.get_appInfo(path + '.apk')
		return render_template('ota/detail.html', nav = nav_list, app = appInfo)
	else:
		abort(404)
	end
	

@ota.route('/download/', defaults={'req_path': ''})
@ota.route('/download/<path:req_path>')
def download(req_path):
# 	下载安装包
	path = os.path.join(OTA_DIR, req_path);
	if os.path.isfile(path):
		return send_file(path, as_attachment=True)
	else:
		abort(404)
	end
	
	
@ota.route('/plist/', defaults={'req_path': ''})
@ota.route('/plist/<path:req_path>.plist')
def plist(req_path):
# 	base64解码时，不足4的倍数，补N个=号在后面, 得到原本的base64字符
	joinCount = len(req_path) % 4
	base64Str = req_path + '=' * joinCount
	pathStr = base64.urlsafe_b64decode(base64Str).decode()
	
	path = os.path.join(OTA_DIR, pathStr);
	appPath = path + ".ipa"
	if os.path.isfile(appPath):
		appInfo = ota_api.get_appInfo(appPath)
		ipaPath = request.scheme + "://" + request.host + url_for('ota.download', req_path = pathStr) + '.ipa'
		return render_template('ota/meta.plist', app = appInfo, app_url = ipaPath)
	else:
		abort(404)
	end


@ota.route('/admin/', defaults={'req_path': ''})
@ota.route('/admin/<path:req_path>/')
def admin(req_path):
	abs_path = os.path.join(OTA_DIR, req_path)
	
	if not os.path.exists(abs_path):
		return abort(404)
	
	list = []
	for fi in os.listdir(abs_path):
		if not fi.startswith('.'):
			file = {}
			file['name'] = fi
			
			if len(req_path) > 0:
				file['page_path'] = req_path + "/" + os.path.splitext(fi)[0]
				file['file_path'] = req_path + "/" + fi
			else:
				file['page_path'] = fi
			
			path = os.path.join(abs_path, fi)
			if os.path.isfile(path):
				file['type'] = 1
			else:
				file['type'] = 2
			list.append(file)
	
	return render_template('ota/admin.html', list = list, dir = re.split('/', req_path))


@ota.route('/delete/', defaults={'req_path': ''})
@ota.route('/delete/<path:req_path>')
def delete(req_path):
	abs_path = os.path.join(OTA_DIR, req_path)
	
	if not os.path.exists(abs_path):
		return abort(404)
	
	os.remove(abs_path)
	
	dirPath= os.path.dirname(abs_path)
	req_path = dirPath[len(OTA_DIR) + 1:]
	return redirect(url_for('ota.admin', req_path=req_path))


@ota.route('/upload', methods=['POST'])
@ota.route('/upload')
def upload():
	req_path = request.form['req_path']
	file = request.files['file']
	filename = secure_filename(file.filename)
	upload_path = os.path.join(OTA_DIR, req_path, filename)
	file.save(upload_path)

	return redirect(url_for('ota.admin', req_path=req_path))




