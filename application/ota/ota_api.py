#coding=utf-8

from config import OTA_APP_ROOT as OTA_DIR
import os, re
import plistlib
import zipfile
import shutil

# 查找app的文件夹，得到导航栏数据
def get_app_folders(filePath):
	nav_list = []
	
	list = os.listdir(filePath)
	for fi in list:
		fi_d = os.path.join(filePath, fi)

		if os.path.isdir(fi_d):
			sub_nav_list = get_app_folders(fi_d)
			
			dict = {}
			dict['name'] = fi  #文件夹名
			if sub_nav_list:
				dict['sub'] = sub_nav_list

			nav_list.append(dict)
	
	return nav_list


# 获取指定文件夹下的app列表
def get_list_inPath(folderPath):
	app_list = []
	
	list = os.listdir(folderPath)
	
	# os.path.getmtime() 函数是获取文件最后修改时间
	# os.path.getctime() 函数是获取文件最后创建时间
	full_list = [os.path.join(folderPath,i) for i in list]
	time_sorted_list = sorted(full_list, key=os.path.getmtime, reverse=True)
	sorted_filename_list = [os.path.basename(i) for i in time_sorted_list]
	
	for fi in sorted_filename_list:
		fi_d = os.path.join(folderPath, fi)
		if os.path.isfile(fi_d) and not fi.startswith('.'):
			appInfo = get_appInfo(fi_d)
			if appInfo:
				app_list.append(appInfo)
	return app_list


# 获取app信息
# 	name:app的名字
# 	id:app的bundle id
# 	version:app的版本
# 	desc:返回的格式为version(x.x.x)， build(xxxxx)
def get_appInfo(filePath):

	listOfFile = os.path.split(filePath)
	
	platform, category = os.path.split(listOfFile[0][len(OTA_DIR) + 1:])
	baseName = listOfFile[-1]
	
	appInfo = {}
	appInfo['platform'] = platform
	appInfo['category'] = category
	appInfo['fileName'] = os.path.splitext(baseName)[0]
	appInfo['fileExtension'] = os.path.splitext(baseName)[-1]
			
	if appInfo['fileExtension'] == '.ipa':
		plist = get_ipa_plist(filePath)
		
		appInfo['type'] = 1
		if 'CFBundleDisplayName' in plist:
			appInfo["name"] = plist['CFBundleDisplayName']
		else:
			appInfo["name"] = plist['CFBundleName']
		appInfo['id'] = plist['CFBundleIdentifier']
		appInfo['version'] = plist['CFBundleShortVersionString'] #plist需要
		appInfo['desc'] = 'version(%s)，build(%s)'%(plist['CFBundleShortVersionString'], plist['CFBundleVersion'])
		
		return appInfo
	elif appInfo['fileExtension'] == '.apk':
		xml = get_apk_xml(filePath)

		appInfo['type'] = 2
		
		appInfo["name"] = appInfo['fileName']
		appInfo['id'] = "com.bozhong.test"
		appInfo['version'] = "4.5.0"
		appInfo['desc'] = 'version(4.4.0)，build(20123231)'

		return appInfo
	else:
		return None
	end


# 获取iOS的plist数据
def get_ipa_plist(filePath):
    app_file = zipfile.ZipFile(filePath)
    pattern = re.compile(r'Payload/[^/]*.app/Info.plist')
    for path in app_file.namelist():
        m = pattern.match(path)
        if m is not None:
        	plist_path = m.group()
        	break
        
    plist_data = app_file.read(plist_path)
    plist_info = plistlib.loads(plist_data)
    return plist_info


# 获取Android的xml数据
def get_apk_xml(filePath):
	return None
