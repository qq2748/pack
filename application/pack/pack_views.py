# coding=utf-8

from flask import render_template, request, abort, redirect, send_file, url_for, Blueprint

from . import pack_api

pack = Blueprint('pack', __name__, template_folder='templates')

#使用flask 框架 搭建接口服务

@pack.route('/', methods=['GET'])
def signin_form():
    
    list = pack_api.getList();

    return render_template('pack/index.html', list = list)


@pack.route('/test', methods=['GET'])
def test():
    return render_template('pack/test.html')