# coding=utf-8

import os
import shutil

import time
from subprocess import getstatusoutput

#打包脚本

ISOTIMEFORMAT = '%Y-%m-%d-%X'

babyPath = "/usr/local/var/www/dl/apps/Android/baby/"
babyProjectPath = "/Users/dist/hg/android/babytracker"
ivfPath = "/usr/local/var/www/dl/apps/Android/ivf/"
ivfProjectPath = "/Users/dist/hg/android/ivfassist"
crazyPath = "/usr/local/var/www/dl/apps/Android/crazy/"
crazyProjectPath = "/Users/dist/hg/android/Crazy_android_studio"
doctorPath = "/usr/local/var/www/dl/apps/Android/doctor/"
doctorProjectPath = "/Users/dist/hg/android/bzdoctor"


def go(path, tag):
    os.chdir(path)
    print("path: " + path)
    print("current path: " + os.getcwd())

    (status, output) = getstatusoutput('hg pull -u')
    print(status, " xixi " + output + " xixi")

    isUpdated = "no changes found" in output
    os.system("hg pull")
    os.system("hg up " + tag)
    # if isUpdated:
    #     print("没有更新 , 不打包")
    # else:
    print("有更新 , 打个新包")
    if (path == babyProjectPath):
        return packet_android(babyPath, "Office")
    if (path == ivfProjectPath):
        return packet_android(ivfPath, "Office")
    if (path == crazyProjectPath):
        return packet_android(crazyPath, "Office")
    if (path == doctorProjectPath):
    	return packet_android(doctorPath,"Office")


def goOnline(path, tag):
    os.chdir(path)
    print("path: " + path)
    print("current path: " + os.getcwd())

    (status, output) = getstatusoutput('hg pull -u')
    print(status, " xixi " + output + " xixi")

    isUpdated = "no changes found" in output
    os.system("hg pull")
    os.system("hg up " + tag)
    # if isUpdated:
    #     print("没有更新 , 不打包")
    # else:
    print("有更新 , 打个新包")
    if path == babyProjectPath:
       return packet_android(babyPath, "Online")
    if path == ivfProjectPath:
       return packet_android(ivfPath, "Online")
    if path == crazyProjectPath:
       return packet_android(crazyPath, "Online")
    if (path == doctorProjectPath):
    	return packet_android(doctorPath,"Online")

def goProduct(path, tag):
    os.chdir(path)
    print("path: " + path)
    print("current path: " + os.getcwd())

    (status, output) = getstatusoutput('hg pull -u')
    print(status, " xixi " + output + " xixi")

    isUpdated = "no changes found" in output
    os.system("hg pull")
    os.system("hg up " + tag)
    # if isUpdated:
    #     print("没有更新 , 不打包")
    # else:
    print("有更新 , 打个新包")
    if path == babyProjectPath:
       return packet_android(babyPath, "")
    if path == ivfProjectPath:
       return packet_android(ivfPath, "")
    if path == crazyProjectPath:
       return packet_android(crazyPath, "")
    if (path == doctorProjectPath):
    	return packet_android(doctorPath,"")

def del_outputs(path):
    try:
        if getFileSize(path) > 9:
            print("apk太多了, 删除一次文件夹")
            shutil.rmtree(path)
        shutil.rmtree(os.getcwd() + "/outputs")
    except OSError:
        print("no need to delete /outputs")


def getFileSize(path):
    count = 0
    for i in os.listdir(path):
        count += 1
    return count


def packet_android(path, evn):
    try:
        del_outputs(path)
        os.system("./gradlew clean")
        result = os.popen("./gradlew assemble" + evn + "Release").read()

        os.chdir(os.getcwd() + "/outputs")

        apkPath = os.path.abspath(os.listdir(os.getcwd())[0])
        try:
            os.mkdir(path)
        except OSError:
            print("xixi it had a dir")
        finally:
            print("now copy file")

        if evn == "":
            evn = "Product"
        new_name = path + evn + "-" + time.strftime('%Y-%m-%d_%H_%M_%S', time.localtime()) + ".apk"

        shutil.copy(apkPath, new_name)
        print("copy success")
        os.system("python3 /usr/local/var/www/dl/lib/setup.py")
        print("xixixi, success, you can download apk from web now!!!!!")
        if "BUILD SUCCESSFUL" in result:
            result = "BUILD SUCCESSFUL"
        return '<br />' + result.replace('\n', '<br />')
    except Exception as err:
        return '<br />' + str(err) + '<br />' + result.replace('\n', '<br />')


def get_path(dir):
    for filename in os.listdir(dir):
        filepath = os.path.join(dir, filename)
        if os.path.isfile(filepath):
            return filepath


def timer(n):
    '''''
    每n秒执行一次
    '''
    while True:
        print(time.strftime('%Y-%m-%d %X', time.localtime()))
        go(babyProjectPath, "tip")
        go(ivfProjectPath, "tip")
        go(crazyProjectPath, "tip")
        time.sleep(n)

# timer(600)

def getSelect(projectPath):
    os.chdir(projectPath)
    popen = os.popen("hg tags")

    lines = popen.readlines()

    list2 = []
    for line in lines:
        list2.append(str(line).split(' ', 1)[0])

    select = '''<select name="tag">'''
    for tag in list2:
        select += "<option>" + tag + "</option>"
    select += '''</select>'''
    return list2


def getList():
    list = []

    selectYj = getSelect(babyProjectPath)
    selectCrazy = getSelect(crazyProjectPath)
    selectIvf = getSelect(ivfProjectPath)
    selectDoctor = getSelect(doctorProjectPath)

    yj = {}
    yj['select'] = selectYj
    yj['action'] = "http://192.168.80.160:5001/tracker"
    yj['button'] = "打包孕迹"

    list.append(yj)

    ivf = {}
    ivf['select'] = selectIvf
    ivf['action'] = "http://192.168.80.160:5001/ivf"
    ivf['button'] = "打包试管"

    list.append(ivf)

    crazy = {}
    crazy['select'] = selectCrazy
    crazy['action'] = "http://192.168.80.160:5001/crazy"
    crazy['button'] = "打包疯狂造人"

    list.append(crazy)

    doctor = {}
    doctor['select'] = selectDoctor
    doctor['action'] = "http://192.168.80.160:5001/doctor"
    doctor['button'] = "打包播种医生"

    list.append(doctor)

    return list;
